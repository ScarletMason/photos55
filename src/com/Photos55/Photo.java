package com.Photos55;

import com.Photos55.Controllers.LoginController;
import com.Photos55.Controllers.SearchController;
import com.Photos55.Model.Album;
import com.Photos55.Model.Image;
import com.Photos55.Model.User;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The main class, handles initially setting up the project
 *
 * @author Mason Grosset
 * @author Jared Neal
 */
public class Photo extends Application {
	/**
	 * The main jfx stage
	 */
	private static Stage mainStage;
	/**
	 * All of the users on the application
	 */
	public static ArrayList<User> users = new ArrayList<>();

	/**
	 * Gives the main jfx stage, used to switch between views
	 *
	 * @return The main javafx stage
	 */
	public static Stage getMainStage() {
		return mainStage;
	}

	/**
	 * Sets up initial view
	 *
	 * @param primaryStage The main stage used by this application
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			if (new File("data.dat").exists())
				//noinspection unchecked
				users = (ArrayList<User>) new ObjectInputStream(new FileInputStream("data.dat")).readObject();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		boolean hasAdmin = false;
		boolean hasStock = false;
		for (User user : users)
			if (user.getName().equals("admin")) hasAdmin = true;
			else if (user.getName().equals("stock")) hasStock = true;
		if (!hasAdmin) users.add(new User("admin", new ArrayList<>()));
		if (!hasStock) {
			ArrayList<Image> stockImages = new ArrayList<>();
			for (int i = 1; i <= 5; i++) {
				stockImages.add(new Image("stock\\stock" + i + ".jpg", "", new ArrayList<>()));
			}
			ArrayList<Album> stockAlbums = new ArrayList<>();
			stockAlbums.add(new Album("stock", stockImages));
			users.add(new User("stock", stockAlbums));
		}
		primaryStage.setTitle("Photos55");
		mainStage = primaryStage;
		new LoginController(users).show();
	}

	/**
	 * Saves all of the application's data
	 */
	public static void save() {
		try {
			//noinspection ResultOfMethodCallIgnored
			new File("data.dat").createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			new ObjectOutputStream(new FileOutputStream("data.dat")).writeObject(users);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Launches the application
	 *
	 * @param args The arguments that will be passed to jfx
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
